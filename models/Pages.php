<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $lang
 * @property string $slug
 * @property integer $parent
 * @property string $content
 * @property string $title
 * @property string $title_plain
 * @property string $date
 * @property string $status
 * @property string $url
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lang'], 'required'],
            [['id', 'parent'], 'integer'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['lang', 'status'], 'string', 'max' => 45],
            [['slug', 'title', 'title_plain', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Lang',
            'slug' => 'Slug',
            'parent' => 'Parent',
            'content' => 'Content',
            'title' => 'Title',
            'title_plain' => 'Title Plain',
            'date' => 'Date',
            'status' => 'Status',
            'url' => 'Url',
        ];
    }
}
