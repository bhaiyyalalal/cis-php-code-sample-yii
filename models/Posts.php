<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $status
 * @property string $title
 * @property string $content
 * @property string $image
 * @property integer $category_id
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'category_id'], 'integer'],
            [['content'], 'string'],
            [['status'], 'string', 'max' => 45],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'status' => 'Status',
            'title' => 'Title',
            'content' => 'Content',
            'image' => 'Image',
            'category_id' => 'Category ID',
        ];
    }
}
