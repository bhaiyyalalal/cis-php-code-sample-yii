<?php

namespace app\controllers;

use Yii;
use app\models\Pages;
use app\models\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @param string $lang
     * @return mixed
     */
    public function actionView($id, $lang)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $lang),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'lang' => $model->lang]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $lang
     * @return mixed
     */
    public function actionUpdate($id, $lang)
    {
        $model = $this->findModel($id, $lang);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'lang' => $model->lang]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $lang
     * @return mixed
     */
    public function actionDelete($id, $lang)
    {
        $this->findModel($id, $lang)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $lang
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $lang)
    {
        if (($model = Pages::findOne(['id' => $id, 'lang' => $lang])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionChangeStatus($id, $lang, $status){
        Yii::$app->db->createCommand('UPDATE pages SET status = :status WHERE id = :id AND lang = :lang', [
            ':status' => $status,
            ':id' => $id,
            ':lang' => $lang
        ])->execute();
        return 'OK';
    }


    public function actionChangeImage($id, $lang){
        $file = UploadedFile::getInstanceByName('file');
        $name = $id.'-'.$lang.'-'.rand(1,99999).'.'.$file->getExtension();
        if($file->saveAs(Yii::getAlias('@app').'/web/images/pages/'.$name)){
            $Page = Pages::find()->where(['id' => $id, 'lang' => $lang])->one();
            if(YII_DEBUG){
                $Page->thumbnail = 'http://bookie-cms.com/images/pages/'.$name;
            } else {
                $Page->thumbnail = 'http://mozbet.com/images/pages/'.$name;
            }
            $Page->save();
            return $Page->thumbnail;
        }
    }
}
