<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */

$this->registerJsFile('/js/pages.js', ['position' => \yii\web\View::POS_END]);

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'lang' => $model->lang], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'lang' => $model->lang], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="col-sm-10">
        <div style="width: 19%; display: inline-block; vertical-align: top">
            <?php if($model->thumbnail): ?>
                <?= Html::img($model->thumbnail) ?>
            <?php else: ?>
                <h3>No image</h3>
            <?php endif; ?>
        </div>

        <div style="width: 40%; display: inline-block; vertical-align: top">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'title_plain',
                    'content:ntext',
                    'date',
                ],
            ]) ?>
        </div>
        <div style="width: 40%; display: inline-block; vertical-align: top">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'lang',
                    'slug',
                    'parent',
                    [
                        'label' => 'status',
                        'value' => Html::dropDownList('status', $model->status, ['new' => 'new', 'publish' => 'publish', 'unpublished' => 'unpublished'], ['class' => 'product-status', 'data-id' => $model->id, 'data-lang' => $model->lang]),
                        'format' => 'raw'
                    ],
                    'url:url',
                ],
            ]) ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="panel panel-default">
            <div class="panel-heading">On different languages</div>
            <div class="panel-body">
                <?php foreach(\app\models\Pages::find()->select('lang, id')->asArray()->where(['title_plain' => $model->title_plain])->all() as $lang): ?>
                    <?= Html::a($lang['lang'], ['view', 'id' => $lang['id'], 'lang' => $lang['lang']]) ?><br>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
    <hr>

    <h2>Contained pages</h2>
    <?php $count = 0 ?>
    <?php foreach(\app\models\Pages::find()->where(['parent' => $model->id, 'lang' => $model->lang])->all() as $slavePost): ?>
        <?php $count++; ?>
        <div class="col-md-6">
            <h3>Children: <?= $slavePost->title ?></h3>
            <?= DetailView::widget([
                'model' => $slavePost,
                'attributes' => [
                    [
                        'label' => 'thumbnail',
                        'value' => "<img src='{$slavePost->thumbnail}' class='width-100'>",
                        'format' => 'html'
                    ],
                    [
                        'label' => 'Change image',
                        'value' => Html::fileInput('image', null, ['class' => 'product-image', 'data-id' => $slavePost->id, 'data-lang' => $slavePost->lang]).'<span style="color:red">Image must be 950x400</span>',
                        'format' => 'raw'
                    ],
                    'id',
                    'title',
                    'date',
                    'lang',
                    [
                        'label' => 'status',
                        'value' => Html::dropDownList('status', $slavePost->status, ['new' => 'new', 'future' => 'future', 'unpublished' => 'unpublished'], ['class' => 'product-status', 'data-id' => $slavePost->id, 'data-lang' => $slavePost->lang]),
                        'format' => 'raw'
                    ],
                ],
            ]) ?>
            <div class="alert alert-success hidden" role="alert">...</div>
            <div class="alert alert-info hidden" role="alert">...</div>
            <div class="alert alert-warning hidden" role="alert">...</div>
            <div class="alert alert-danger hidden" role="alert">...</div>
        </div>
        <?php if($count%2 == 0): ?>
            <div style="clear: both;"></div>
        <?php endif; ?>
    <?php endforeach; ?>

</div>
