<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="card-box">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Posts', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            //'page_id',
            'title',
            [
                'attribute' => 'lang',
                'filter' => ['rus' => 'rus', 'eng' => 'eng', 'arm' => 'arm', 'chi' => 'chi', 'jpn' => 'jpn', 'kor' => 'kor', 'por' => 'por', 'spa' => 'spa', 'tur' => 'tur']
            ],
            [
                'attribute' => 'status',
                'filter' => ['published' => 'Published', 'unpublished' => 'Unpublished']
            ],
            [
                'attribute' => 'category_id',
                'filter' => \app\models\Categories::getCategoriesList($searchModel->lang?$searchModel->lang:'all'),
                'content' => function($m){
                    return $m->category_title;
                }
            ],
            //'content:ntext',
            // 'image',
            // 'category_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
