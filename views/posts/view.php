<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Posts */

$this->registerJsFile('/js/posts.js');

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>v
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'page_id',
            'title',
            'lang',
            'content:html',
            [
                'label' => 'status',
                'value' => Html::dropDownList('status', $model->status, ['new' => 'new', 'published' => 'published', 'unpublished' => 'unpublished'], ['class' => 'product-status', 'data-id' => $model->id, 'data-lang' => $model->lang]),
                'format' => 'raw'
            ],
            'category_title',
            [
                'attribute' => 'image',
                'value' => Html::img($model->image),
                'format' => 'html'
            ]
        ],
    ]) ?>

</div>
